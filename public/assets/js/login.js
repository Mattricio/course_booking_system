console.log('hello from loginjs')

document.querySelector("#show-login").addEventListener("click", function (){
	document.querySelector(".popup").classList.add("active");
});

document.querySelector(".popup .close-btn").addEventListener("click", function (){
	document.querySelector(".popup").classList.remove("active");
});


//lets target our form component section inside our document
let loginForm = document.querySelector('#loginUser')

// the login form will be used by the client to insert his/her account to be authenticated by the app
loginForm.addEventListener("submit", (pagpasok) => {
	pagpasok.preventDefault()

	//lets capture the values of our form components
let email = document.querySelector("#userEmail").value
let pass = document.querySelector("#password").value	

// checkers to confirm the acquired values
// console.log(email);
// console.log(pass);

// our next task is to validate the data inside the input fields first
	if (email == "" || pass== "") {
	alert("Please input your email and/or password first")
	} else { // send a request to the desired endpoint
		fetch("https://powerful-garden-15579.herokuapp.com/api/users/login", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify ({
				email: email,
				password: pass
			})
		}).then(res => {
			return res.json()
		}).then(dataConverted => {
			// checks if conversion of the response has been successful
			console.log(dataConverted);
			// save the generated access token inside the local storage property of the browser
			if (dataConverted.accessToken) {
					localStorage.setItem('token', dataConverted.accessToken)
					// created this alert as a confirmation of the previous task
					// alert("successfully generated access token")

					// Make sure that before you redirect the user to the next ocation you have to indentify the access rights that you will grant for that user

					fetch("https://powerful-garden-15579.herokuapp.com/api/users/details", {
						headers: {
							// lets pass on the value of our access
							'Authorization': `Bearer ${dataConverted.accessToken}`
						} // upon sending this request, we are instantiating a promise that can lead to 2 possible outcomes
					}).then(res => {
						return res.json()
					}).then(data => {
						//check if we are able to get the user's data
						console.log(data)
						// fetching the user details/ info is a success
						// save the "id", "isAdmin" properties
						localStorage.setItem("id",data._id )
						localStorage.setItem("isAdmin", data.isAdmin)
						// as developers, its up to you to create a checker for the values
						// console.log('items are saved')
						window.location.replace('./profile.html')
					})

					// how would you redirect the user to another location in your app
					
			} else {
				// this block of code will run if no access token was generated
				alert("Password/Username Incorrect")
			}

			
			
		})
	}
})