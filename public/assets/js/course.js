// lets identify which course this page needs to display inside the browser
// earlier, we passed the course id data inside the parameters of the url for us to quickly identify which course to diplay
// how are we going to get the value passed inside the url parameters
// the answer is to use a URLSearchParams() function, this method/constructor creates and returns a URLSearchParams object

// window.location -> returns a location object with information about the "current" location of the document
//.search -> contains the query string section of the current URL

let urlValues = new URLSearchParams(window.location.search)
// let userToken = localStorage.getItem("token")
let id = urlValues.get('courseId')
let token = localStorage.getItem('token')
// lets create a control structure that will determine the display inside the navbar if there is a user currently logged in the app

/*if (!token) {
	window.location.replace("./login.html")
} else {*/
// lets check what the structure of this variable will look like
console.log(urlValues) // checkers


console.log(id) // checkers

// lets capture the value of the access token inside the lcoal storage


// console.log(token) for checking only

// lets set first a container for all the details that we would want to display
let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")

// lets send a request going to the endpoint that will allow us to get and display the course details
fetch(`https://powerful-garden-15579.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
	console.log(convertedData)

	name.innerHTML = convertedData.name 
	price.innerHTML = convertedData.price
	desc.innerHTML = convertedData.description
	enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

	document.querySelector("#enrollButton").addEventListener("click", () => {

		fetch(`https://powerful-garden-15579.herokuapp.com/api/users/details`, {
			method:'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.json()).then(jsonData => {
			let userCourses = jsonData.enrollments.map(courses => {
				return courses.courseId
			})
			if (userCourses.includes(id)) {
				alert('you are already enrolled in this course!')
			} else {

		//insert the course inside the the enrollments array of the user
		fetch(`https://powerful-garden-15579.herokuapp.com/api/users/enroll`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`//we have to get the actual value of the token variable
			},
			body: JSON.stringify({
				courseId: id
			})
		})
		.then(res => {
			return res.json()
		}).then(convertedResponse => {
			console.log(convertedResponse)
			// lets create a control structure that will determine a response according to the resulst that will be displayed to the client
			if (convertedResponse === true) {
				alert('Thank your enrolling')
				// we can redirect the user back to the courses page
				window.location.replace('./profile.html')
			}
			 
			})
			}
		})
	})
})
/*}*/