// what will be the first task in retrieving all records inside the course collection
let adminControls = document.querySelector('#adminButton')
// target the container from the html document
let container = document.querySelector('#coursesContainer')

// lets determine if there is a user currently logged in
// lets capture one of the properties that is currently stored in our web storage
const isAdmin = localStorage.getItem("isAdmin")
// lets create a control structure to determine the display in the front end
let userToken = localStorage.getItem("token")

// lets create a control structure that will determine the display inside the navbar if there is a user currently logged in the app

if (!userToken) {
	window.location.replace("./login.html")
} else {
if (isAdmin == "false" || !isAdmin) {	
	adminControls.innerHTML = null 
} else {
	adminControls.innerHTML = `
		<div class="col-md-2 offset-md-10">
			<a href="../pages/addCourse.html" class="btn btn-block btn-warning">Add Course</a>
		</div>			
	`
}

// send a request to retrieve all documents from the courses collection
fetch('https://powerful-garden-15579.herokuapp.com/api/courses/').then(res => res.json()).then(jsonData => {
	console.log(jsonData)

//lets declare a variable that will display the result in the browser depending on the return
let courseData;

// create a control structure that will determine the value that the variable will hold

if (jsonData.length < 1) {
	console.log('No Courses Available')
	courseData = "No Courses Available"
	container.innerHTML = courseData
} else {
// if the given condition is not met, display the contents of the array inside our page
	courseData = jsonData.map(course => {
		// lets check the make up/structure of each element inside the courses collection
			console.log(course._id)
			console.log(course.name)
			//lets use template literals to pass or inject the properties of our object inside each section of a card component

			// lets fix the current behavior of our courses page 
			let cardFooter;
			// what if i want to pass the value of a property fro the course object inside the url?
			if (isAdmin == "false" || !isAdmin) {
				cardFooter = 
				`
				<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
				`
			} else {
				cardFooter = 	
				`
					<a href="" class="btn btn-success text-white btn-block">Edit Course</a>
					<a href="" class="btn btn-danger text-white btn-block">Delete Course</a>
				`
			}
			return (
				 `
			<div class="col-md-3 my-3">
				<div class="card bg-dark text-light">
					<div class="card-body">
						<h3 class="card-title">Course Name: ${course.name}</h3>
						<p class="card-text text-left">Price: ${course.price}</p>
						<p class="card-text text-left"> Desc: ${course.description}</p>
						<p class="card-text text-left">Created On: ${course.createdOn}</p>
					</div>
					<div class="class-footer">
						${cardFooter}
					</div>
				</div>
			</div>`
				)
			 	
			
		}).join("") // we will use this join method to create
			container.innerHTML = courseData;
	}
})
}