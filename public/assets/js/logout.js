console.log('hello from logout')

// upon logging out the, the credentials of the user should be removed from the local storage

// the clear method will wipe out/remove the contents of the local storage
localStorage.clear();

// upon clearing out the local storage, redirect the user back to the log in page

window.location.replace("./login.html")

//INDIVIDUAL TASKS:
// Apply all the necessary changes in the navbarof each page
// STRETCH GOAL
// create a logic that will  disable the profile link navbar component if an admin is logged in
// the admin should be re-directed to the courses page only