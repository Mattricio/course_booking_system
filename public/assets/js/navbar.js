console.log('hello from navbarjs')

let navItem = document.querySelector("#navSession")

// how will we know if there is a user currently logged in?
let navUserToken = localStorage.getItem("token")

// lets create a control structure that will determine the display inside the navbar if there is a user currently logged in the app

if (!navUserToken) {
	navItem.innerHTML = `
	<li class="nav-item">
		<a href="./login.html" class="nav-link">Log in</a>
	</li>
	` 
} else {
	navItem.innerHTML = `
	<li class="nav-item">
		<a href="./Logout.html" class="nav-link">Log out</a>
	</li>
	` 
}



