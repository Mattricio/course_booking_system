	// create our add course page
let userToken = localStorage.getItem("token")

// lets create a control structure that will determine the display inside the navbar if there is a user currently logged in the app

if (!userToken) {
	window.location.replace("./login.html")
} else {
	
let formSubmit = document.querySelector('#createCourse')

// create a logic that will make sure that the needed information is sufficient from our form component. make sure to inform the user what went wrong

// lets acquire an event that will be applied in our form component
// create a subfunction inside the method to describe the procedure/action that will take place upon triggering the event
formSubmit.addEventListener("submit", (mangyayari) => {
	mangyayari.preventDefault() // preventDefaulat will avoid page redirection

	// lets target the values of each component inside the forms
	let name = document.querySelector("#courseName").value
	let cost = document.querySelector("#coursePrice").value
	let desc = document.querySelector("#courseDesc").value

	// lets create a checker to see if we were able to capture the values of each input field
	console.log(name)
	console.log(cost)
	console.log(desc)

	// send a request to the backend project to process the data for creating a new entry inside our courses collection.	

	// send a request to the backend project to process the data for creating a new entry inside our ourses collection
	if(name !== "" && cost !== "" && desc !== "") {
		// should i allow the user to create a new document
		// upon creating this fetch api request, we are instantiating a promise
	fetch('https://powerful-garden-15579.herokuapp.com/api/courses/course-exists', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify ({
			name: name
		})
	}).then(res => {
		return res.json()
	}).then(convertedData => {
		console.log(convertedData)
		if (convertedData === false) {
		fetch('https://powerful-garden-15579.herokuapp.com/api/courses/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				// what are the properties of the document that the user needs to fill?
				//pass the the values of the form component inside the request body via using the name, desc, and cost variables
				name: name,
				description: desc,
				price: cost
			})
		}).then(res => {
			console.log(res)
			return res.json() // always convert the response into json format
		}).then(info => {
			console.log(info)
			// create a control structure that will desribe the the response into json format
			if (info === true ) {
				Swal.fire("Course created successfully")
			} else {
				Swal.fire('Error')
		}
	})

		} else {
			Swal.fire('Course already exists')
		}
	})



	} else {
		Swal.fire('Fill out all fields')

	}	
}) 
}
