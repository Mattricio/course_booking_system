console.log('hello from profile js');

// get the value of the access token inside the local storage and place it inside a new variable

let token = localStorage.getItem("token")
console.log(token)

const isAdmin = localStorage.getItem("isAdmin")

let lalagyan = document.querySelector("#profileContainer")

// let userToken = localStorage.getItem("token")

// lets create a control structure that will determine the display inside the navbar if there is a user currently logged in the app

// if (!token) {
// 	window.location.replace("./login.html")
// } else {

if (isAdmin == "true" || !isAdmin) {	
	window.location.replace("./courses.html") 
} else {

// the goal is to display the information about the user details by sending a request to the back-end project
fetch("https://powerful-garden-15579.herokuapp.com/api/users/details", {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
}).then(res => res.json())
.then(jsonData => {	

	// get all the elements inside the enrollments array.


	//how are we going to display the info inside the front end component
	// lets target the div element first using its id attribute
	lalagyan.innerHTML = `
		<div class="container">
		<div class="col-md-12 my-3">
			<section class="jumbotron bg-dark text-light" style="padding: 1px; margin: 1px;">
				<h3 class="text-center" style="font-size:20px;">First Name: ${jsonData.firstName}</h3>
				<h3 class="text-center" style="font-size:20px;">Last Name: ${jsonData.lastName}</h3>
				<h3 class="text-center" style="font-size:20px;">Email: ${jsonData.email}</h3>
				<h3 class="text-center" style="font-size:20px;">Mobile Number: ${jsonData.mobileNo}</h3>
				<table class="table">
					<tr class="text-light">
						<th>Course Name: </th>
						<th>Course Description: </th>
						<th>Enrolled On: </th>
						<th>Status: </th>
						<tbody id="courseBody"></tbody>
					</tr>
				</table>
			<section/>
		<div/>
		</div>
	`

for (let i=0; i<jsonData.enrollments.length; i++) {
		fetch(`https://powerful-garden-15579.herokuapp.com/api/courses/${jsonData.enrollments[i].courseId}`,{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}})
		.then(res => res.json())
		.then(courseData => {
			document.getElementById('courseBody').innerHTML +=
			`
				<tr>
					<td class="text-center text-light" style="font-size:15px;">${courseData.name}</td>
					<td class="text-center text-light" style="font-size:15px;"> ${courseData.description}</td>
					<td class="text-center text-light" style="font-size:15px;"> ${jsonData.enrollments[i].enrolledOn}</td>
					<td class="text-center text-light" style="font-size:15px;"> ${jsonData.enrollments[i].status}</td>
				</tr>
			`
		});
	}
})
}
// }
