	// console.log("hello from JS file");


document.querySelector("#show-signup").addEventListener("click", function (){
	document.querySelector(".popup").classList.add("active");
});

document.querySelector(".popup .close-btn").addEventListener("click", function (){
	document.querySelector(".popup").classList.remove("active");
});

// lets target our form component and place it inside a new variable
let registerForm = document.querySelector("#registerUser")

// inside the first parameter of the method describe event that it will listen to
// inside th 2nd parameter, lets describe the action or procedure that will happen upon triggering the first (parameter) event 
// preventDefault will avoid page refresh or page re-direction once pangyayari has been triggered
registerForm.addEventListener("submit", (pangyayari) => {
	pangyayari.preventDefault()

// capture each values inside the input fields first


// capture each values inside input fields first, then repackage them inside a new variable
// lets create a checker to make sure that we are successful in capturing the values
let firstName = document.querySelector("#firstName").value
console.log(firstName)
let lastName = document.querySelector("#lastName").value
console.log(lastName)
let email = document.querySelector("#userEmail").value
console.log(email)
let mobileNo = document.querySelector("#mobileNumber").value
console.log(mobileNo)
let password = document.querySelector("#password1").value
console.log(password)
let verifyPassword = document.querySelector("#password2").value
console.log(verifyPassword)

// lets create a simple alert message just to inform the user of a response
// alert("successfully captured data")
// the fire method will you to trigger the pop up box
// what if i want to create different sections if my pop up box
/*Swal.fire({
	//describe the structure of the card inside an object
	icon: "error",
	title: "Noooo!",
	text: "Error in registering."
	})*/

	// lets create a data validation for our register page. WHY?
	// why do we need to validate data? .. to check and verify if the data that we will accept is accuracy
	// we do data validation to make sure that the storage space will be properly stored

	// the ff data/info that we can validate 
	// email, password. mobileNo
	// lets create a control structure to determine the next set of procedures before the user can register a new account
	// it will be a lot more efficient to sanitize the data before submitting it to backend/server for processing

	//stretch goals: (these are not required)
	// => the password should contain both alphanumeric numbers and at least one special character
	// => find a way so that the mobile number will be able to accept both foreign and local mobile number formats 

	if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && verifyPassword !== "") && (password === verifyPassword) && (mobileNo.length === 11)){
		
		//to integrate the email exists method; send out a new request
		// before you allow the user to create a new account, check if the email value is still available; this will ensure that each user will have their unique user email
		//upon sending this requst, we are instantiating a promise object, which means we should apply a method to handle the response
		fetch("https://powerful-garden-15579.herokuapp.com/api/users/email-exists", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify ({
				email: email
			})
		}).then(res => {
			return res.json() // to make it parseable/readable once the response returns to the client side
		}).then(convertedData => { //what would the response look like
			// lets create a control structure to determine the proper procedure depending of the response
			if (convertedData === false) {
				// lets allow the user to register an account
				fetch('https://powerful-garden-15579.herokuapp.com/api/users/register', {
			// we will now describe the structure of our request for register:
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			// API only accepts request in a string format. we should stringify JS script for postman
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		}).then(res => {
			console.log(res)
			return res.json()
		}).then(data => {
			console.log(data)
			// lets create a control structure to give out a proper response depending on the return from the backend
			if(data === true) {
				Swal.fire("New Account registered successfully!")
			}else{
			// inform the user that something went wrong
			Swal.fire("Something went wrong during in the registration.")
				}
			}
			)
		} else {
				Swal.fire("The Email already exists.")
			}
		}
		)

		// this block of code will run if the condition has been met
		// how can we create a new account for user using the data that he/she entered?
		// url -> describes the destination of the request
		// 3 states for a promisew (pending, fulfilled, rejected)
		
	}else{
		Swal.fire("Please fill out all fields");
	}
})

// Make the DIV element draggable:
// dragElement(document.getElementById("mydiv1"));

// function dragElement(elmnt) {
//   var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
//   if (document.getElementById(elmnt.id + "header")) {
//     // if present, the header is where you move the DIV from:
//     document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
//   } else {
//     // otherwise, move the DIV from anywhere inside the DIV:
//     elmnt.onmousedown = dragMouseDown;
//   }

//   function dragMouseDown(e) {
//     e = e || window.event;
//     e.preventDefault();
//     // get the mouse cursor position at startup:
//     pos3 = e.clientX;
//     pos4 = e.clientY;
//     document.onmouseup = closeDragElement;
//     // call a function whenever the cursor moves:
//     document.onmousemove = elementDrag;
//   }

//   function elementDrag(e) {
//     e = e || window.event;
//     e.preventDefault();
//     // calculate the new cursor position:
//     pos1 = pos3 - e.clientX;
//     pos2 = pos4 - e.clientY;
//     pos3 = e.clientX;
//     pos4 = e.clientY;
//     // set the element's new position:
//     elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
//     elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
//   }

//   function closeDragElement() {
//     // stop moving when mouse button is released:
//     document.onmouseup = null;
//     document.onmousemove = null;
//   }
// }
